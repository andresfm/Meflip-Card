package Helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import java.util.UUID;

import co.ferdroid.meflipcard.R;

/**
 * Created by Andres Fernando M. on 28/10/16.
 */

public class Util {

    private Context context;
    private String ok_btn;
    private String cancel_btn;

    private String service_url;


    public Util(Context context) {
        this.context = context;
    }


    public String getService_url() {
        return service_url;
    }

    public void setService_url() {
        this.service_url = "";
    }


    public String getOk_btn() {
        return ok_btn;
    }

    public void setOk_btn(String ok_btn) {
        this.ok_btn = ok_btn;
    }

    public String getCancel_btn() {
        return cancel_btn;
    }

    public void setCancel_btn(String cancel_btn) {
        this.cancel_btn = cancel_btn;
    }


    public void setDialogButtons(String ok_btn, String cancel_btn) {
        this.ok_btn = ok_btn;
        this.cancel_btn = cancel_btn;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }




    public void showAlert(String title, String message, DialogInterface.OnClickListener positiveAnswer, DialogInterface.OnClickListener negativeAnswer, int cancelable) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setMessage(message);

        if (positiveAnswer!=null) {
            alert.setPositiveButton(this.ok_btn, positiveAnswer);
        } else {
            alert.setPositiveButton(this.ok_btn, null);
        }

        if (negativeAnswer!=null) {
            alert.setNegativeButton(this.cancel_btn, negativeAnswer);
        }

        if (cancelable == 1) {
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Log.d("@@@ evt cancel alert ", "alert cancel");
                    ((Activity)context).finish();
                }
            });
        } else {
            alert.setCancelable(false);
        }

        alert.show();
    }



    public void showPromt(View promptView, String title, String message, DialogInterface.OnClickListener positiveAnswer, DialogInterface.OnClickListener negativeAnswer) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setView(promptView);
        if (positiveAnswer!=null) {
            alert.setPositiveButton(this.getOk_btn(), positiveAnswer);
        } else {
            alert.setPositiveButton(this.getOk_btn(), null);
        }
        if (negativeAnswer!=null) {
            alert.setNegativeButton(this.getCancel_btn(), negativeAnswer);
        }
        alert.show();
    }


    public String getDevice_Id(ContentResolver cntr, Context bctx) {
        String android_id = Settings.Secure.getString(cntr, Settings.Secure.ANDROID_ID);
        TelephonyManager tm = (TelephonyManager) bctx.getSystemService(Context.TELEPHONY_SERVICE);
        String tmDevice = "" + tm.getDeviceId();
        String tmSerial = "" + tm.getSimSerialNumber();
        UUID deviceUuid = new UUID(android_id.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }



    public ProgressDialog showProgress(Context ctx, String progressTxt) {
        ProgressDialog progress1 = new ProgressDialog(ctx);
        progress1.setMessage(progressTxt);
        progress1.setCancelable(false);
        return progress1;
    }





}
