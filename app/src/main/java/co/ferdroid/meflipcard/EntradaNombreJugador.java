package co.ferdroid.meflipcard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class EntradaNombreJugador extends AppCompatActivity {

    EditText player1, player2;
    private RadioGroup rdg_turno;
    private RadioButton opt_turno;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada_nombre_jugador);


        player1 = (EditText) findViewById(R.id.player1);
        player2 = (EditText) findViewById(R.id.player2);

        rdg_turno = (RadioGroup) findViewById(R.id.rdg_turno);


    }




    public void prepararTablero(View vi) {

        String p1 = player1.getText().toString().trim();
        String p2 = player2.getText().toString().trim();

        String[] players = {p1, p2};


        if (p1.equals("") || p2.equals("")) {
            Toast.makeText(this, getString(R.string.msg_fill_names), Toast.LENGTH_SHORT).show();
        } else if (rdg_turno.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, getString(R.string.msg_select_turn), Toast.LENGTH_SHORT).show();
        } else {

            int turno_seleccionado = rdg_turno.getCheckedRadioButtonId();

            opt_turno = (RadioButton) findViewById(turno_seleccionado);
            //Log.d("## turno seleccionado ", opt_turno.getTag().toString());

            Intent it_juego = new Intent(this, MainActivity.class);
            it_juego.putExtra("players_name", players);
            it_juego.putExtra("turn", Integer.parseInt(opt_turno.getTag().toString()));

            startActivity(it_juego);

            finish();
        }

    }



}
